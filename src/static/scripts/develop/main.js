

//home toggle buttons
$(function(){
	$('.toggle').on('click', function(){ 
		if($(this).hasClass("toggle-plus")){
			$(this).removeClass("toggle-plus");
			$(this).addClass("toggle-minus");
			$(this).closest("div").find(".faq__desc").addClass("open");
		}
		else{
			$(this).removeClass("toggle-minus");
			$(this).addClass("toggle-plus");
			$(this).closest("div").find(".faq__desc").removeClass("open");
		}
	});
});
/* Prevent default submit and go to the next step. Change this behavior as needed on prod */
$(function() {
    $('form[data-next-step]').submit(function(e) {
        e.preventDefault();
        var nextStep = this.dataset.nextStep;
        document.location = nextStep;
    });
})


function validate(formSubmit){
    var form = $(formSubmit).closest('form'),
        check_groups = $('.required');

    form.addClass('validate');    
    
    if(check_groups.length){
        check_groups.each(function(){
            var group = $(this),
                checks = group.find('[type="checkbox"]'),
                validateChecks = function(){                    
                    if(group.find(':checked').length > 0){
                        checks.each(function(){
                            $(this).removeAttr("required");
                        });
                    }
                    else{
                        checks.each(function(){
                            $(this).attr("required", true);
                        });
                    }
                };
            validateChecks();
            checks.on("change", function(){
                validateChecks();
            })
        });
    }
}