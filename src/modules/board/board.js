$(function () {
	(function setBoardBg() {
		var board = $('.board'),
			boardBg = board.data("bg");
		board.css("background-image", "url(" + boardBg + ")");
	})();

	function fillSpace() {
		var wh = $(window).height(),
			dh = $('body').height(),
			diff = wh - dh,
			board = $('.board'),
			bh = board.outerHeight(),
			page = $('.page'),
			ph = page.outerHeight();
		console.log(wh, dh, diff, bh, bh + diff, ph, ph + diff);
		if (diff > 0) {
			if (board.length) {
				board.css("min-height", bh + diff);
				console.log(board.length);
			}
			else {
				page.css("min-height", ph + diff);
				console.log(page.length);
			}
		}
	};
	$(window).on("resize load", fillSpace);

});