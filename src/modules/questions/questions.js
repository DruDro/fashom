/* Redirect Male to home-page */

$(function () {
    $('#genderMale').change(function () {
        if (this.value === "male") {
            var popLink = '<a href="#infoPop" class="js-popup hidden"></a>';
            $(popLink).prependTo('.questions').click().remove();
        }
    });
    $(document).on('mouseup touchend', function (e) {
        if ($(e.target).hasClass('popup-wrapper') && $(e.target).children('#infoPop').length) {
            goHome();
        }
    });
});

function goHome() {    
    window.location.href = '/';
}




// colorRadios
$('input.color').each(function () {
    var radio = $(this);
    radio.css("background-color", radio.val());
    radio.after('<span class="checked"></span>');
});
