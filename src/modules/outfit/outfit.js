$(function(){
	$('.subscription-options__option').each(function(){
		var bg = $(this).data("bg");
		$(this).css("background-image","url(" + bg + ")")
	});
	$(document).on("change", ".keep-radios [type='radio']:checked", function(){
		var item = $(this);
		console.log(item.val());
		if(item.val() == "return"){
			item.closest('.item__details').find('.item__reason').show();
		}
		else {
			item.closest('.item__details').find('.item__reason').hide();			
		}
	});
	$(".keep-radios [type='radio']").each(function(){ $(this).trigger("change")});

});
